import { GestOfLifPage } from './app.po';

describe('gest-of-lif App', () => {
  let page: GestOfLifPage;

  beforeEach(() => {
    page = new GestOfLifPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
