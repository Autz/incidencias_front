# Pages
En esta carpeta se ubican todas las "páginas" o vistas de la aplicación y se encontrará estructurado de la siguiente manera:

1) Cada tipo de usuario poseerá una ***CARPETA*** en está sección Ej. Administrador, Gerente, etc.
2) Cada carpeta de usuario poseerá un ***MÓDULO*** que será el encargado de:
    a) Administrar las rutas de ese usuario Ej. admin/dir_1, admin/dir_2, etc.
    b) Administrar los componentes (que en nuestro caso representarán las páginas)
3) Cada capeta de usuario debe poseer una carpeta para almacenar los ***Componentes***
4) Cada usuario debe poseer un archivo para almacenar las ***rutas***.

Quedando la carpeta ***Pages*** con una estructura similar a esta:

```
+-- pages
|   +-- Admin
|   |   +-- admin.module.ts
|   |   +-- admin.routes.ts
|   |   +-- components
|   |   |   +-- component_1
|   |   |   |   +-- ...
|   +-- Gerente
|   |   +-- gerente.module.ts
|   |   +-- gerente.routes.ts
|   |   +-- components
|   |   |   +-- component_1
|   |   |   |   +-- ...
```
 