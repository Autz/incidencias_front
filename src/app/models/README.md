# Models
En esta carpeta se encuentran los "modelos" de la aplicación que no son más que una representación de la base de datos en forma de clases o interfaces (preferiblemente interfaces)

## importante
Si un tipo de usuario necesita de más de una tabla y esta "estructura de datos" se hace un poco "compleja" se deberá crear una carpeta con el nombre del tipo de usuario y dentro del nuevo directorio crear una interfaz (con un nombre significativo) que contenga dicha estructura de datos.